acpi-call (1.1.0-6) unstable; urgency=medium

  * [ce9dd75] Fix patches order (#3 was applied first)
  * [01a2d16] Add patch to fix build on Linux 5.6.
    Thanks to Nikita Knutov for the patch, Thomas Koch and Gard Spreemann for
    pointing me to the patch (Closes: #959364, #959729)
  * [6ca6f81] Bump Standards-Version to 4.5.0
  * [f14a922] Bump debhelper compat to 12
  * [93b030a] Silence lintian about spelling error (false positive)

 -- Raphaël Halimi <raphael.halimi@gmail.com>  Thu, 07 May 2020 14:58:58 +0200

acpi-call (1.1.0-5) unstable; urgency=medium

  * [302afec] Migrate project to Salsa
  * [a412748] Use secure copyright file specification URI.
    Thanks to Jelmer Vernooĳ
  * [c633d47] Add patch to fix memory leak.
    Thanks to Colin Ian King for the patch (Closes: #929316) (LP: #1829883)
  * [8f5f753] Don't build module when ACPI is disabled.
    Thanks to Seth Forshee, Thadeu Lima de Souza Cascardo, Michael Jeanson and
    others for ideas and testing (LP: #1830040)
  * [757be2b] Bump Standards-Version to 4.3.0
  * [1e7e83a] Override lintian warning about missing test suite

 -- Raphaël Halimi <raphael.halimi@gmail.com>  Fri, 31 May 2019 17:33:13 +0200

acpi-call (1.1.0-4) unstable; urgency=low

  * [45b90c6] Fix build failure on 4.12.
    Include the correct header for copy_from_user prototype
    Thanks to Colin Ian King for the patch (Closes: #868110)
    (LP: #1700783, #1705678)
  * [ba63573] Bump Standards-Version to 4.0.0, no changes needed
  * [7640fff] Bump debhelper compat to 10
  * [ea4dd52] Drop build-dependency on dpkg-dev

 -- Raphaël Halimi <raphael.halimi@gmail.com>  Mon, 24 Jul 2017 20:58:44 +0200

acpi-call (1.1.0-3) unstable; urgency=low

  * Update Vcs-* fields
  * Update debian/watch file
  * Add debian/gbp.conf
  * Bump Standards-Version to 3.9.7, no changes required

 -- Raphaël Halimi <raphael.halimi@gmail.com>  Mon, 21 Mar 2016 18:07:27 +0100

acpi-call (1.1.0-2) unstable; urgency=medium

  * Add patch to fix compatibility with Linux 3.17 (Closes: #762281)
  * Add Vcs-Browser and Vcs-Git fields to debian/control

 -- Raphaël Halimi <raphael.halimi@gmail.com>  Sun, 21 Sep 2014 03:43:48 +0200

acpi-call (1.1.0-1) unstable; urgency=low

  * Initial release (Closes: #760338)

 -- Raphaël Halimi <raphael.halimi@gmail.com>  Wed, 03 Sep 2014 04:30:34 +0200
